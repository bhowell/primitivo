#!/usr/bin/env python3
import configparser
import sqlite3
import os

import bottle

config = configparser.ConfigParser()
config.read("primitivo.cfg")
TEMPLATE = config["SITE"]["template"]
PORT = config["SITE"]["port"]

db = sqlite3.connect("data/db.sqlite")
dbc = db.cursor()

app = bottle.app()

viewpath = os.path.join("views", TEMPLATE)
staticpath = os.path.join("views", TEMPLATE, "static")
bottle.TEMPLATE_PATH.append(viewpath)


def get_page(pageid):
    dbc.execute("select * from pages where id=?", (pageid,))
    return dbc.fetchone()


def create_page(pageid):
    page = get_page(pageid)
    if page:
        return page
    else:
        content = "Enter your page content here."
        dbc.execute("insert into pages(id, content) values(?, ?)", (pageid,
                                                                    content))
        db.commit()
        return get_page(pageid)


def delete_page(pageid):
    if pageid == "index":
        print("ERROR!  You can't delete the index page!")
        return
    dbc.execute("delete from pages where id=?", (pageid,))
    db.commit()

    render_static()


def update_page(pageid, content):
    dbc.execute("update pages set content=? where id=?", (content, pageid))
    db.commit()

    # render static pages
    render_static()


def render_static():
    pages = get_pages()
    for p in pages:
        pname = p[0]
        page = get_page(pname)
        out = bottle.template("page", page=page, pages=pages)
        odir = os.path.join("output", "p", pname)
        if not os.path.exists(odir):
            os.makedirs(odir)
        ofile = os.path.join(odir, "index.html")
        fo = open(ofile, "w")
        # fo.write(out.encode('utf8'))  # this was for python2
        fo.write(out)


def get_pages():
    dbc.execute("select * from pages")
    return dbc.fetchall()


@bottle.route("/")
def index():
    pages = get_pages()
    page = get_page("index")
    return bottle.template("page", page=page, pages=pages)


@bottle.route("/p/:pageid")
def page(pageid):
    page = get_page(pageid)
    pages = get_pages()
    if page:
        return bottle.template("page", page=page, pages=pages)
    else:
        bottle.abort(404, "I still haven't found what you're looking for.")


@bottle.route("/admin")
def admin():
    pages = get_pages()
    return bottle.template("admin", pages=pages)


@bottle.route("/admin/e/:pageid")
def edit(pageid):
    if pageid:
        page = get_page(pageid)
    else:
        page = ("", "")

    return bottle.template("edit", page=page)


@bottle.post("/admin/new")
def new():
    postd = bottle.request.forms
    pageid = postd.pageid
    create_page(pageid)
    bottle.redirect("/admin/e/" + pageid)


@bottle.post("/admin/save")
def save():
    postd = bottle.request.forms
    pageid = postd.pageid
    pagecontent = postd.pagecontent
    update_page(pageid, pagecontent)
    bottle.redirect("/admin")


@bottle.route("/admin/del/:pageid")
def delete(pageid):
    delete_page(pageid)
    bottle.redirect("/admin")


@bottle.route("/static/<filename:path>")
def server_static(filename):
    # print staticpath + "/" + filename
    return bottle.static_file(filename, root=staticpath)


@bottle.route("/js/<filename:path>")
def server_static_js(filename):
    return bottle.static_file(filename, root="js")


def main():
    bottle.run(app=app, quiet=False, reloader=True, port=PORT)

if __name__ == "__main__":
    main()
