%include("sciphi/header.tpl")
<div id="content">
  <div id="menu">
  <ul>
    <li>&nbsp;</li>
  </ul>
  </div>
  <div id="ctext">
       <h4>Site Administration</h4>
       <table id="pageadmin">
         <caption>Pages</caption>
    %for page in pages:
         <tr>
           <td>{{page[0]}}</td>
           <td><a href="/admin/del/{{page[0]}}">Delete</a></td>
           <td><a href="/admin/e/{{page[0]}}">Edit</a></td>
         </tr>  
    %end
       </table>

    <br />   
    <form action="/admin/new" method="POST">
      New Page: <input type="text" name="pageid" size="30" />
    <input type="submit" value="Create" />
    </form>
  </div>
</div>

%include("sciphi/footer.tpl")
