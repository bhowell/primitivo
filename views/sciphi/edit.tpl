%include("sciphi/header.tpl")
<div id="content">
  <div id="menu">
  <ul>
    <li><a href="/admin">Site Admin</a></li>
    <br /><br /><br /><br />
    <h6>click on the text here to start editing -></h6>
  </ul>
  </div>
  <div id="ctext">
    <form id="editform" action="/admin/save" method="POST">
    %if page[0] == "":
       <h4>Create Page</h4>
       Name: <input type="text" size="30" name="pageid" />
    %else:
       <h4>Edit Page: {{page[0]}}</h4>
       <input type="hidden" name="pageid" value="{{page[0]}}" />
       <input type="hidden" name="pagecontent" id="contentfield" value="" />
       <div id="pageContents">{{!page[1]}}</div>
    %end
    <input type="submit" value="Save" id="saveedit" />
    </form>
  </div>
</div>
<link href="/js/alloy-editor/assets/alloy-editor-ocean-min.css" rel="stylesheet">
<script src="/js/alloy-editor/alloy-editor-all-min.js"></script>
<script>
  AlloyEditor.editable('pageContents');

  var button = {
    clicked: false,

    click: function(){
      this.clicked = true;
      // assert(button.clicked, "The button has been clicked");
      contents = document.getElementById("pageContents").innerHTML;
      field = document.getElementById("contentfield");
      field.value = contents;
    }
  };

  btn = document.getElementById("saveedit");
  btn.addEventListener("click", button.click, false);
  
</script>
<!--script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.20/angular.min.js" type="text/javascript"></script>
<script src="http://cdn.jsdelivr.net/g/angular.textangular@1.2.2(textAngular-sanitize.min.js+textAngular.min.js)" type="text/javascript"></script>
<script>
      angular.module("editor", ['textAngular']);
      function editController($scope) {
          $scope.htmlContent = '{{!page[1]}}';
      };
</script -->

%include("sciphi/footer.tpl")
